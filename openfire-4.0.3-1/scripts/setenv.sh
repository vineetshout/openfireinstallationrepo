#!/bin/sh
echo $PATH | egrep "/opt/openfire-4.0.3-1/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/opt/openfire-4.0.3-1/java/bin:/opt/openfire-4.0.3-1/php/bin:/opt/openfire-4.0.3-1/mysql/bin:/opt/openfire-4.0.3-1/apache2/bin:/opt/openfire-4.0.3-1/common/bin:$PATH"
export PATH
fi
echo $LD_LIBRARY_PATH | egrep "/opt/openfire-4.0.3-1/common" > /dev/null
if [ $? -ne 0 ] ; then
LD_LIBRARY_PATH="/opt/openfire-4.0.3-1/mysql/lib:/opt/openfire-4.0.3-1/apache2/lib:/opt/openfire-4.0.3-1/common/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
fi

TERMINFO=/opt/openfire-4.0.3-1/common/share/terminfo
export TERMINFO
##### JAVA ENV #####
JAVA_HOME=/opt/openfire-4.0.3-1/java
export JAVA_HOME

##### PHP ENV #####
PHP_PATH=/opt/openfire-4.0.3-1/php/bin/php
export PHP_PATH
##### MYSQL ENV #####

##### APACHE ENV #####

##### CURL ENV #####
CURL_CA_BUNDLE=/opt/openfire-4.0.3-1/common/openssl/certs/curl-ca-bundle.crt
export CURL_CA_BUNDLE
##### SSL ENV #####
SSL_CERT_FILE=/opt/openfire-4.0.3-1/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/opt/openfire-4.0.3-1/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/opt/openfire-4.0.3-1/common/lib/engines
export OPENSSL_ENGINES


. /opt/openfire-4.0.3-1/scripts/build-setenv.sh
