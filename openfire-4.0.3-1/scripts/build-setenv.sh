#!/bin/sh
LDFLAGS="-L/opt/openfire-4.0.3-1/common/lib $LDFLAGS"
export LDFLAGS
CFLAGS="-I/opt/openfire-4.0.3-1/common/include $CFLAGS"
export CFLAGS
CXXFLAGS="-I/opt/openfire-4.0.3-1/common/include $CXXFLAGS"
export CXXFLAGS
		    
PKG_CONFIG_PATH="/opt/openfire-4.0.3-1/common/lib/pkgconfig"
export PKG_CONFIG_PATH
