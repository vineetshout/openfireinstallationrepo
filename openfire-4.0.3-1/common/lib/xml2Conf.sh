#
# Configuration file for using the XML library in GNOME applications
#
XML2_LIBDIR="-L/opt/openfire-4.0.3-1/common/lib"
XML2_LIBS="-lxml2 -lz   -liconv -lm "
XML2_INCLUDEDIR="-I/opt/openfire-4.0.3-1/common/include/libxml2"
MODULE_VERSION="xml2-2.9.1"

