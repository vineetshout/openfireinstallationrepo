#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/opt/openfire-4.0.3-1/common/lib"
XSLT_LIBS="-lxslt  -L/opt/openfire-4.0.3-1/common/lib -lxml2 -lz -liconv -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/opt/openfire-4.0.3-1/common/include"
MODULE_VERSION="xslt-1.1.28"
