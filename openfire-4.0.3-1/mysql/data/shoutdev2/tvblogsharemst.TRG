TYPE=TRIGGERS
triggers='CREATE DEFINER=`shouttech`@`%` TRIGGER `shoutdev2`.`ShareBlogDTime` BEFORE INSERT\n    ON shoutdev2.tvblogsharemst FOR EACH ROW\nBEGIN\n   SET new.datetime = CONVERT_TZ(NOW(), @@session.time_zone, \'+00:00\');\nEND'
sql_modes=1073741824
definers='shouttech@%'
client_cs_names='utf8'
connection_cl_names='utf8_general_ci'
db_cl_names='utf8_general_ci'
