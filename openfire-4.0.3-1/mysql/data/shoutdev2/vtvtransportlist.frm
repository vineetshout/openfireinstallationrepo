TYPE=VIEW
query=select `b`.`memberid` AS `memberid`,`a`.`transportcode` AS `transportcode`,`a`.`transportname` AS `transportname`,`a`.`transportdesc` AS `transportdesc`,`a`.`transportkeywords` AS `transportkeywords`,`a`.`isGeneric` AS `isGeneric`,`a`.`iconURL` AS `iconURL`,`b`.`gclat` AS `memgclat`,`b`.`gclng` AS `memgclng`,degrees(((acos(((sin(radians(`a`.`latitude`)) * sin(radians(`b`.`gclat`))) + ((cos(radians(`a`.`latitude`)) * cos(radians(`b`.`gclat`))) * cos(radians((`a`.`longitude` - `b`.`gclng`)))))) * 60) * 1.1515)) AS `distance` from (`shoutdev2`.`tvtransportmaster` `a` join `shoutdev2`.`kprusermst` `b`) where (`a`.`transportcode` <> 0)
md5=df443428fcaef0bab767ec2104595432
updatable=1
algorithm=0
definer_user=shouttech
definer_host=%
suid=2
with_check_option=0
timestamp=2017-06-16 07:00:28
create-version=1
source=SELECT `b`.`memberid`  AS `memberid`,\n   		  `a`.`transportcode` AS `transportcode`,\n          `a`.`transportname`     AS `transportname`,\n		  `a`.`transportdesc`     AS `transportdesc`,\n		  `a`.`transportkeywords`     AS `transportkeywords`,\n		  `a`.`isGeneric`     AS `isGeneric`,\n          `a`.`iconURL`  AS `iconURL`,\n		  `b`.`gclat`  AS `memgclat`,\n		  `b`.`gclng`  AS `memgclng`	,\n		 degrees(((acos(((sin(radians(`a`.`latitude`)) * sin(radians(`b`.`gclat`))) + \n	 ((cos(radians(`a`.`latitude`)) * cos(radians(`b`.`gclat`))) *\n	  cos(radians((`a`.`longitude` - `b`.`gclng`)))))) * 60) * 1.1515)) AS `distance`  \n     FROM (`shoutdev2`.`tvtransportmaster` `a` JOIN `shoutdev2`.kprusermst `b`)\n	 Where (`a`.`transportcode`!=0)
client_cs_name=utf8
connection_cl_name=utf8_general_ci
view_body_utf8=select `b`.`memberid` AS `memberid`,`a`.`transportcode` AS `transportcode`,`a`.`transportname` AS `transportname`,`a`.`transportdesc` AS `transportdesc`,`a`.`transportkeywords` AS `transportkeywords`,`a`.`isGeneric` AS `isGeneric`,`a`.`iconURL` AS `iconURL`,`b`.`gclat` AS `memgclat`,`b`.`gclng` AS `memgclng`,degrees(((acos(((sin(radians(`a`.`latitude`)) * sin(radians(`b`.`gclat`))) + ((cos(radians(`a`.`latitude`)) * cos(radians(`b`.`gclat`))) * cos(radians((`a`.`longitude` - `b`.`gclng`)))))) * 60) * 1.1515)) AS `distance` from (`shoutdev2`.`tvtransportmaster` `a` join `shoutdev2`.`kprusermst` `b`) where (`a`.`transportcode` <> 0)
