TYPE=VIEW
query=select `b`.`journeyid` AS `journeyid`,`b`.`journeyshout` AS `journeyshout`,`b`.`shoutid` AS `shoutid`,`a`.`memberid` AS `memberid`,`a`.`fname` AS `fname`,`a`.`lname` AS `lname`,`a`.`imageurl` AS `imageurl`,`b`.`activitycode` AS `activitycode`,`c`.`activityname` AS `activityname`,`c`.`iconURL` AS `activityiconURL`,`b`.`geocode` AS `geocode`,`b`.`longitute` AS `longitute`,`b`.`lattitude` AS `lattitude`,`b`.`crdttm` AS `crdttm`,`b`.`shout` AS `shout`,`b`.`gclat` AS `gclat`,`b`.`gclng` AS `gclng`,`b`.`stype` AS `stype`,`b`.`linkurl` AS `linkurl`,`b`.`taggedusers` AS `taggedusers` from ((`shoutdev2`.`kprshouts` `b` join `shoutdev2`.`kprusermst` `a`) join `shoutdev2`.`tvactivitymaster` `c`) where ((`b`.`journeyshout` = 1) and (`b`.`memberid` = `a`.`memberid`) and (`b`.`activitycode` = `c`.`activitycode`))
md5=2c1b53c4b11eec14e3a16a122f957979
updatable=1
algorithm=0
definer_user=shouttech
definer_host=%
suid=2
with_check_option=0
timestamp=2017-06-16 09:44:48
create-version=1
source=SELECT  `b`.`journeyid` AS `journeyid`,\n		   `b`.`journeyshout` AS `journeyshout`,\n		   `b`.`shoutid` AS `shoutid`,\n		   `a`.`memberid`  AS `memberid`,\n		   `a`.`fname`  AS `fname`,\n		   `a`.`lname`  AS `lname`,\n		   `a`.`imageurl`  AS `imageurl`,\n		   `b`.`activitycode` AS `activitycode`,\n		   `c`.`activityname` AS `activityname`,\n		   `c`.`iconURL` AS `activityiconURL`,\n		   `b`.`geocode` AS `geocode`,\n		   `b`.`longitute` AS `longitute`,\n		   `b`.`lattitude` AS `lattitude`,\n		   `b`.`crdttm` AS `crdttm`,\n		   `b`.`shout` AS `shout`,\n		   `b`.`gclat` AS `gclat`,\n		   `b`.`gclng` AS `gclng`,\n		   `b`.`stype` AS `stype`,\n		   `b`.`linkurl` AS `linkurl`,\n		   `b`.`taggedusers` AS `taggedusers`\n     FROM ((`shoutdev2`.`kprshouts` `b` JOIN `shoutdev2`.kprusermst `a`) \n	 JOIN `shoutdev2`.tvactivitymaster `c`) \n	 WHERE ((`b`.`journeyshout`=true) \n	 AND (`b`.`memberid` = `a`.`memberid`) \n	 AND (`b`.`activitycode` = `c`.`activitycode`))
client_cs_name=utf8
connection_cl_name=utf8_general_ci
view_body_utf8=select `b`.`journeyid` AS `journeyid`,`b`.`journeyshout` AS `journeyshout`,`b`.`shoutid` AS `shoutid`,`a`.`memberid` AS `memberid`,`a`.`fname` AS `fname`,`a`.`lname` AS `lname`,`a`.`imageurl` AS `imageurl`,`b`.`activitycode` AS `activitycode`,`c`.`activityname` AS `activityname`,`c`.`iconURL` AS `activityiconURL`,`b`.`geocode` AS `geocode`,`b`.`longitute` AS `longitute`,`b`.`lattitude` AS `lattitude`,`b`.`crdttm` AS `crdttm`,`b`.`shout` AS `shout`,`b`.`gclat` AS `gclat`,`b`.`gclng` AS `gclng`,`b`.`stype` AS `stype`,`b`.`linkurl` AS `linkurl`,`b`.`taggedusers` AS `taggedusers` from ((`shoutdev2`.`kprshouts` `b` join `shoutdev2`.`kprusermst` `a`) join `shoutdev2`.`tvactivitymaster` `c`) where ((`b`.`journeyshout` = 1) and (`b`.`memberid` = `a`.`memberid`) and (`b`.`activitycode` = `c`.`activitycode`))
