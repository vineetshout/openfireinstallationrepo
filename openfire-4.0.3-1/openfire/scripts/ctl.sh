#!/bin/sh

OPENFIRE_PIDFILE="/opt/openfire-4.0.3-1/openfire/openfire.pid"
OPENFIRE_HOME="/opt/openfire-4.0.3-1/openfire"
OPENFIRE_LOGDIR="/opt/openfire-4.0.3-1/openfire/logs"
JAVA_HOME="/opt/openfire-4.0.3-1/java"
JAVACMD="${JAVA_HOME}/bin/java"

if [ "$(id -u)" = "0" ]; then
    OPENFIRE_USER="openfire"
else
    OPENFIRE_USER="$USER"
fi

# Prepare location of openfire libraries
OPENFIRE_LIB="${OPENFIRE_HOME}/lib"

# Prepare openfire command line
OPENFIRE_OPTS="${OPENFIRE_OPTS} -DopenfireHome=${OPENFIRE_HOME} -Dopenfire.lib.dir=${OPENFIRE_LIB}"

# Prepare local java class path
if [ -z "$LOCALCLASSPATH" ]; then
    LOCALCLASSPATH="${OPENFIRE_LIB}/startup.jar"
else
    LOCALCLASSPATH="${OPENFIRE_LIB}/startup.jar:${LOCALCLASSPATH}"
fi

# Export any necessary variables
export JAVA_HOME JAVACMD

# Lastly, prepare the full command that we are going to run.

OPENFIRE_RUN_CMD="${JAVACMD} -server ${OPENFIRE_OPTS} -classpath ${LOCALCLASSPATH} -jar ${OPENFIRE_LIB}/startup.jar"

get_pid() {
    PID=""
    PIDFILE=$1
    #check for pidfile
    if [ -f "$PIDFILE" ]; then
        PID=`cat $PIDFILE`
    fi
}

get_openfire_pid(){
    get_pid $OPENFIRE_PIDFILE
    if [ ! "$PID" ]; then
        return
    fi
    if [ "$PID" -gt 0 ]; then
        OPENFIRE_PID=$PID
    fi
}

is_service_running() {
    PID=$1
    if [ "x$PID" != "x" ] && kill -0 $PID 2>/dev/null; then
        RUNNING=1
    else
        RUNNING=0
    fi
    return $RUNNING
}

is_openfire_running() {
    get_openfire_pid
    is_service_running $OPENFIRE_PID
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        OPENFIRE_STATUS="openfire not running"
    else
        OPENFIRE_STATUS="openfire already running"
    fi
    return $RUNNING
}

start_openfire(){
    is_openfire_running
    RUNNING=$?

    OLD_PWD=`pwd`
    cd $OPENFIRE_LOGDIR

    if [ $RUNNING -eq 1 ]; then
        echo "$0 $ARG: openfire (pid $OPENFIRE_PID) already running"
    else
        if [ "$OPENFIRE_USER" = "openfire" ]; then
            su - $OPENFIRE_USER -c "nohup $OPENFIRE_RUN_CMD > $OPENFIRE_LOGDIR/nohup.out 2>&1 &"
        else
            nohup $OPENFIRE_RUN_CMD > $OPENFIRE_LOGDIR/nohup.out 2>&1 &
        fi
        if [ $? -eq 0 ]; then
            echo "`ps axf | grep java | grep openfire | awk '{print $1}'`" > $OPENFIRE_PIDFILE
            echo "$0 $ARG: openfire started at port 9090"
        else
            echo "$0 $ARG: openfire could not be started"
            ERROR=3
        fi
    fi
    sleep 1
    cd $OLD_PWD
}

cleanpid() {
    rm -f $OPENFIRE_PIDFILE
}

stop_openfire() {
    NO_EXIT_ON_ERROR=$1
    is_openfire_running
    RUNNING=$?

    if [ $RUNNING -eq 0 ]; then
        echo "$0 $ARG: $OPENFIRE_STATUS"
        if [ "x$NO_EXIT_ON_ERROR" != "xno_exit" ]; then
            exit
        else
            return
        fi
    else
        kill $OPENFIRE_PID
    fi
    sleep 5
    is_openfire_running
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        echo "$0 $ARG: openfire stopped"
        cleanpid
    else
        echo "$0 $ARG: openfire could not be stopped"
        ERROR=4
    fi
}

if [ "x$1" = "xstart" ]; then
    start_openfire
elif [ "x$1" = "xstop" ]; then
    stop_openfire
elif [ "x$1" = "xstatus" ]; then
    is_openfire_running
    echo "$OPENFIRE_STATUS"
elif [ "x$1" = "xcleanpid" ]; then
    cleanpid
fi

exit $ERROR
