import org.jivesoftware.util.*;

public class DecryptDatabaseParameter
{
    public static void main(String[] args) {
        Encryptor encryptorInterface = new AesEncryptor();
        String decrypted = encryptorInterface.decrypt(args[0]);
        System.out.println(decrypted);
    }
}
