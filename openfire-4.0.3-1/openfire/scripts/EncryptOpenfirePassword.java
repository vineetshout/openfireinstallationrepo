import org.jivesoftware.util.Blowfish;
import org.jivesoftware.openfire.auth.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.security.sasl.SaslException;
import javax.xml.bind.DatatypeConverter;

public class EncryptOpenfirePassword
{
    public static void main(String[] args) {
        SecureRandom random = new SecureRandom();
        byte[] saltShaker = new byte[24];
        String encryptedPassword = null;
        byte[] saltedPassword = null, clientKey = null, storedKey = null, serverKey = null;
        Blowfish cipher = new Blowfish(args[1]);


        random.nextBytes(saltShaker);
        String salt = DatatypeConverter.printBase64Binary(saltShaker);
        try {
            saltedPassword = ScramUtils.createSaltedPassword(saltShaker, args[0], 4096);
            clientKey = ScramUtils.computeHmac(saltedPassword, "Client Key");
            storedKey = MessageDigest.getInstance("SHA-1").digest(clientKey);
            serverKey = ScramUtils.computeHmac(saltedPassword, "Server Key");
        } catch (SaslException | NoSuchAlgorithmException e) {
            System.out.println("Error found");
        }
        encryptedPassword = cipher.encryptString(args[0]);


        System.out.println("UPDATE ofUser SET encryptedPassword='" + encryptedPassword + "', storedKey='" + DatatypeConverter.printBase64Binary(storedKey) + "', serverKey='" + DatatypeConverter.printBase64Binary(serverKey) + "', salt='" + salt + "', iterations=4046 WHERE username='admin'");
    }
}
