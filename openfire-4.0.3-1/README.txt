Bitnami Openfire Stack 4.0.3-1
		  ============================

1. OVERVIEW

The Bitnami Project was created to help spread the adoption of freely available, 
high quality, open source web applications. Bitnami aims to make it easier than 
ever to discover, download and install open source software such as document and 
content management systems, wikis and blogging software.

You can learn more about Bitnami at https://bitnami.com

Openfire is a real-time collaboration server based on the XMPP (Jabber) 
protocol. Openfire is easy to administer and is optimized for security and 
performance.

You can learn more about Openfire at
http://www.igniterealtime.org/projects/openfire/index.jsp

The Bitnami Openfire Stack is an installer that greatly simplifies the
installation of Openfire and runtime dependencies. It includes ready-to-run
version of Java and MySQL.

Bitnami Openfire Stack is distributed for free under the Apache 2.0 license. 
Please see the appendix for the specific licenses of all open source components 
included.

Please see the appendix for the specific licenses of all Open Source
components included.

You can learn more about Bitnami Stacks at https://bitnami.com/stacks/

2. FEATURES

- Easy to Install

Bitnami Stacks are built with one goal in mind: to make it as easy as
possible to install open source software. Our installers completely automate
the process of installing and configuring all of the software included in
each Stack, so you can have everything up and running in just a few clicks.

- Independent

Bitnami Stacks are completely self-contained, and therefore do not interfere
with any software already installed on your system. For example, you can
upgrade your system's MySQL or Apache Tomcat without fear of 'breaking' your
Bitnami Stack.

- Integrated

By the time you click the 'finish' button on the installer, the whole stack
will be integrated, configured and ready to go.

- Relocatable

Bitnami Stacks can be installed in any directory. This allows you to have
multiple instances of the same stack, without them interfering with each other.

3. COMPONENTS

Bitnami Openfire Stack ships with the following software versions:

  - Openfire 4.0.3
  - Apache 2.4.23
  - MySQL 5.6.33
  - Java 1.8.0_101

4. REQUIREMENTS

To install Bitnami Openfire Stack you will need:

    - Intel x86 or compatible processor
    - Minimum of 1024 MB RAM
    - Minimum of 400 MB hard drive space
    - TCP/IP protocol support
    - Compatible operanting systems:
    - An x86 or x64 Linux operating system.

5. INSTALLATION

The Bitnami Openfire Stack is distributed as a binary executable installer.
It can be downloaded from:

https://bitnami.com/stacks/

The downloaded file will be named something similar to:

bitnami-openfire-4.0.3-1-linux-installer.run on Linux or
bitnami-openfire-4.0.3-1-linux-x64-installer.run on Linux x64.

On Linux, you will need to give it executable permissions:

chmod 755 bitnami-openfire-4.0.3-1-linux-installer.run on Linux


To begin the installation process, invoke from a shell or double-click on
the file you have downloaded, and you will be greeted by the 'Welcome'
screen. You will be asked to choose the installation folder. If the
destination directory does not exist, it will be created as part of the
installation.

The default listening port for the bundled Apache is 8080 and for the bundled MySQL is 3006.

The next screen will ask you for a password. This password will be used to
protect the MySQL root account.

Once the installation process has been completed, you will see the
'Installation Finished' page. You can choose to launch Bitnami
Openfire Stack at this point. If you do so, your default web browser
will point you to the Bitnami local site.

If you receive an error message during installation, please refer to
the Troubleshooting section.

The rest of this guide assumes that you installed Bitnami Openfire Stack
in /home/user/openfire-4.0.3-1 on Linux
and you use port 8080 for Apache and 3306 for MySQL.

6. STARTING AND STOPPING BITNAMI OPENFIRE STACK

To enter to the Admin Panel you can point your browser to
http://127.0.0.1:8080/

To start/stop/restart application on Linux you can use the included
ctlscript.sh utility, as shown below:

       ./ctlscript.sh (start|stop|restart)
       ./ctlscript.sh (start|stop|restart) mysql
       ./ctlscript.sh (start|stop|restart) apache
       ./ctlscript.sh (start|stop|restart) openfire

  start      - start the service(s)
  stop       - stop  the service(s)
  restart    - restart or start the service(s)

That will start MySQL, Openfire and Apache services. Once started, you can
open your browser and access the following URL:

http://127.0.0.1:8080/
If you selected an alternate port during installation, for example 8181, the
URL will look like:

http://127.0.0.1:8181/


7. DIRECTORY STRUCTURE

The installation process will create several subfolders under the main
installation directory:

    apache2: Apache Web server.
    java/: Java SE Development Kit.
    mysql/: MySQL Database.
    openfire/: Openfire folder

Inside openfire/ there are additional folders and some files:

       bin/: Openfire's binaries.
       bnconfig: Bitnami tool for Openfire.
       conf/ : Openfire's configuration files.
       changelog.html: Openfire's changelog
       documentation: Openfire's documentation.
       lib/: Libraries for Openfire.
       LICENSE.html: Openfire's license file.
       logs/: Openfire's logs.
       plugins/: Plugins for Openfire.
       resources/: Openfire's resources.
       LICENSE.html: Openfire's license file.
       scripts/: Openfire's initialization scripts.

8. DEFAULT USERNAMES AND PASSWORDS

The default user of the Openfire Server is 'admin', and its password is
the one you set at installation time.

MySQL admin user is called 'root', and its password is the one you set at
installation time.


9. TROUBLESHOOTING

In addition to the resources provided below, we encourage you to post your
questions and suggestions at:

https://community.bitnami.com/

We also encourage you to sign up for our newsletter, which we'll use to
announce new releases and new stacks. To do so, just register at:
https://bitnami.com/newsletter.

9.1 Installer

# Installer Payload Error

If you get the following error while trying to run the installer from the
command line:

"Installer payload initialization failed. This is likely due to an
incomplete or corrupt downloaded file"

The installer binary is not complete, likely because the file was
not downloaded correctly. You will need to download the file and
repeat the installation process.

# Installer execution error on Linux

If you get the following error while trying to run the installer:

"Cannot open bitnami-openfire-4.0.3-1-linux.run: No application suitable for
automatic installation is available for handling this kind of file."

In some operatings systems you can change permissions with right click ->
properties -> permissions -> execution enable.

Or from the command line:

$ chmod 755 bitnami-openfire-4.0.3-1-linux.run

# Customize application on Linux

9.2 Apache

If you find any problem starting Apache, the first place you should check is
the Apache error log file:

/home/user/openfire-4.0.3-1/apache2/logs/error.log

Most errors are related to not being able to listen to the default port.
Make sure there are no other server programs listening at the same port
before trying to start Apache. Some programs, such as Skype, may also use
port 80. For issues not covered in this Quick Start guide, please refer to
the Apache documentation, which is located at http://httpd.apache.org/docs/

9.3 MySQL

If you encounter any problems starting MySQL, the first place to
look in is the "Problems and Common Errors" section of the MySQL
manual, which you will find at http://dev.mysql.com/doc/ The
following are some common problems:

# Access denied when trying to connect to MySQL.

If you get an Access Denied message while trying to connect to
MySQL, make sure you are using the correct username and password.

# "Can't connect to server" message.

Make sure that the MySQL daemon is up and running. Remember also that if
during installation you selected a different listening port for MySQL, you
may need to pass that as an extra command line option.

9.4 Openfire

For any problem related to Openfire server, please visit
https://community.igniterealtime.org/welcome

10. LICENSES

Apache Web Server is distributed under the Apache License v2.0, which
is located at http://www.apache.org/licenses/LICENSE-2.0

MySQL is distributed under the GNU General Public License v2, which is
located at http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

Java and related libraries are distributed under the (BCL) Oracle Binary
Code License Agreeme, which is located at
http://www.oracle.com/technetwork/java/javase/terms/license/index.html

Openfire is distributed under the Open Source Apache License v2.0, which
is located at http://www.apache.org/licenses/LICENSE-2.0
