============ CHANGELOG =============

Version 2.8.0-3      2017-08-15
* Updated Git to 2.14 in OS-X (Fix CVE-2017-1000117)

Version 2.8.0-2      2017-08-11
* Updated Git to 2.14 in Windows and Linux (Fix CVE-2017-1000117)
* Updated MongoDB to 3.4.7

Version 2.8.0-0      2017-08-07
* Updated phpMyAdmin to 4.7.2
* Updated Nginx to 1.12.1
* Updated Node.js to 6.11.1
* Updated Apache to 2.4.27
* Updated MongoDB to 3.4.6
* Updated PHP to 7.0.21
* Updated MariaDB to 10.1.25
* Updated DreamFactory to 2.8.0

Version 2.7.0-1      2017-06-16
* Keep the original .env-dist

Version 2.7.0-0      2017-06-12
* Updated DreamFactory to 2.7.0
* Fix sudo vulnerability (CVE-2017-1000367)
* Updated OpenSSL to 1.0.2l
* Updated PostgreSQL to 9.6.3
* Updated PHP to 7.0.18
* Updated Nodejs to 6.10.3
* Updated MariaDB to 10.1.24
* Updated MongoDB to 3.4.4
* Updated Java to 1.8.0_131
* Updated Nginx to 1.12.0

Version 2.6.0-0      2017-05-02
* Updated DreamFactory to 2.6.0
* Updated MongoDB to 3.4.3
* Updated phpMyAdmin to 4.7.0
* Updated Node.js to 6.10.1

Version 2.5.0-1      2017-03-28
* Added some DreamFactory changes

Version 2.5.0-0      2017-03-13
* Updated DreamFactory to 2.5.0
* Updated Node.js to 4.8.0
* Updated PostgreSQL to 9.6.2
* Updated phpMyAdmin to 4.6.6
* Updated MongoDB to 3.4.2
* Updated Nginx to 1.10.3
* Updated OpenSSL to 1.0.2k (Security fixes for CVE-2017-3731, CVE-2017-3731, CVE-2017-3731, and CVE-2016-7055)
* Updated Java to 1.8.0_121

Version 2.4.2-0      2017-01-31
* Updated DreamFactory to 2.4.2

Version 2.4.1-3      2017-01-25
* Updated PHP to 7.0.15 (CVE-2017-5340)
* Updated MariaDB to 10.1.21 (Security release CVE-2016-6664 CVE-2017-3238)
* Updated MongoDB to 3.4.1
* Updated MariaDB to 10.1.20
* Updated Apache to 2.4.25
* Updated MySQL to 5.6.35
* Updated phpMyAdmin to 4.6.5.2

Version 2.4.1-2      2016-12-07
* Use the correct email for VMs and CIs

Version 2.4.1-1      2016-12-03
* Reduce installer size

Version 2.4.1-0      2016-12-02
* Updated DreamFactory to 2.4.1
* Updated MongoDB to 3.4.0
* Updated phpMyAdmin to 4.6.5.1
* Updated PHP to 5.6.28
* Updated MariaDB to 10.1.19 (CVE-2016-6663 CVE-2016-6664)

Version 2.3.1-0      2016-10-17
* Updated DreamFactory to 2.3.1
* Updated PHP to 7.0.12

Version 2.3.0-5      2016-10-07
* Updated MariaDB to 10.1.18
* Updated MongoDB to 3.2.10
* Updated PostgreSQL to 9.6.0

Version 2.3.0-4      2016-09-29
* Updated OpenSSL to 1.0.2j (Security fix CVE-2016-6304)
* Updated MariaDB to 10.1.17
* Updated PHP to 7.0.11
* Updated Nodejs to 4.5.0

Version 2.3.0-2      2016-09-14
* Fix an issue where you couldn't change your name or email after logging in as user@example.com

Version 2.3.0-1      2016-09-08
* Fix MySQL additional service

Version 2.3.0-0      2016-08-29
* Updated DreamFactory to 2.3.0
* Added PHP 7.0.9
* Added Nodejs 4.4.7
* Updated phpMyAdmin to 4.6.4
* Updated PostgreSQL to 9.5.4
* Updated MongoDB to 3.2.9
* Patched Off-Path TCP Linux Kernel Vulnerability in Cloud Images and Virtual Machines (Security release CVE-2016-5696)
* Updated MariaDB to 10.1.16

Version 2.2.1-2      2016-07-25
* Updated PHP to 5.6.24 (Security release: HTTP_PROXY is improperly trusted by some PHP libraries and applications)
* Blocked Proxy request header in the web server (Security issue CVE-2016-5385, CVE-2016-5387, CVE-2016-1000110)
* Updated MongoDB to 3.2.8

Version 2.2.1-0      2016-07-13
* Updated DreamFactory to 2.2.1
* Updated Apache to 2.4.23
* Updated phpMyAdmin to 4.6.3
* Updated PHP to 5.6.23
* Updated MongoDB to 3.2.7

Version 2.2.0-0      2016-06-06
* Updated DreamFactory to 2.2.0
* Updated MySQL to 5.6.31
* Updated Nginx to 1.10.1 (Security release CVE-2016-4450)
* Updated phpMyAdmin to 4.6.2
* Updated PHP to 5.6.22 (Security release CVE-2016-5096, CVE-2016-5094, CVE-2013-7456, CVE-2016-5093)
* Updated MariaDB to 10.1.14
* Updated PostgreSQL to 9.5.3
* Updated OpenSSL to 1.0.2h (Security release CVE-2016-2108 CVE-2016-2107)

Version 2.1.2-1      2016-05-03
* Updated DreamFactory to 2.1.2
* Updated Nginx to 1.10.0
* Updated PageSpeed to 1.9.32.14
* Updated MongoDB to 3.2.6
* Updated PHP to 5.6.20
* Updated MariaDB to 10.1.13
* Updated PostgreSQL to 9.5.2
* Updated phpMyAdmin to 4.6.0

Version 2.1.1-2      2016-03-31
* Fixed VM problem with cached encryption keys
* Updated PostgreSQL to 9.5.1

Version 2.1.1-1      2016-03-25
* Included .git folder
* Included SOAP extension for Windows

Version 2.1.1-0      2016-03-22
* Updated DreamFactory to 2.1.1
* Updated MariaDB to 10.1.10
* Updated phpMyAdmin to 4.5.5.1
* Updated PHP to 5.6.19
* Updated OpenSSL to 1.0.2g (Security release CVE-2016-0800)
* Updated MongoDB to 3.2.3
* Updated PHP to 5.6.18
* Fixed issue on Windows with curl using wrong certificate file.

Version 2.1.0-4      2016-02-12
* Updated DreamFactory to 2.1.0
* Ldap php extension enabled by default on all platforms.
* Updated phpMyAdmin to 4.5.4.1
* Updated OpenSSL to 1.0.2f
* Updated MySQL to 5.6.29

Version 2.0.4-2      2016-01-19
* Fixed DYLD_LIBRARY_FALLBACK for OSX El Capitan 10.11.1
* Updated MongoDB to 3.2.1
* Updated v8 VC11 for Windows
* Updated libpng to 1.5.26 (Security release CVE-2015-8540)

Version 2.0.4-0      2016-01-08
* Updated DreamFactory to 2.0.4

Version 2.0.3-0      2015-12-31
* Updated DreamFactory to 2.0.3
* Updated Apache to 2.4.18
* Updated Nginx to 1.9.8
* Updated MongoDB to 3.2.0
* Updated MySQL to 5.6.28

Version 2.0.2-0      2015-12-06
* Updated DreamFactory to 2.0.2
* Updated OpenSSL to 1.0.1q
* Updated phpMyAdmin to 4.5.2
* Updated NGINX to 1.9.7

Version 2.0.1-1      2015-11-17
* Updated libpng to 1.5.24 (security issue)

Version 2.0.1-0      2015-10-29
* Updated DreamFactory to 2.0.1
* Included NGINX 1.9.6 to Unix versions

Version 2.0.beta-4      2015-10-23
* Fixed issue shutting down MariaDB
* Updated MongoDB to 3.0.7
* Updated PostgreSQL to 9.4.5
* Updated Apache to 2.4.17

Version 2.0.beta-3      2015-10-13
* Added SQLite as additional service

Version 2.0.beta-2      2015-09-17
* Updated PHP to 5.5.29
* Updated phpMyAdmin to 4.4.13.1

Version 1.9.4-3      2015-08-14
* Applied security patch for DreamFactory 1.9.4
* Updated Apache to 2.4.16
* Updated PHP to 5.5.28

Version 1.9.4-2      2015-07-10
* Updated OpenSSL to 1.0.1p
* Updated FreeTDS to get locales configuration from installdir/common/etc/locales.conf file

Version 1.9.4-1      2015-07-02
* Updated PHP to 5.5.26
* Updated the DreamFactory admin console to 1.0.12

Version 1.9.4-0      2015-06-08
* Updated DreamFactory to 1.9.4
* Updated MySQL to 5.6.25

Version 1.9.2-1      2015-05-29
* Updated PHP to 5.5.25
* Updated phpMyAdmin to 4.4.7
* Dropping RC4 from Apache SSLCipherSuite directive
* Solved issues in upgrade method

Version 1.9.2-0      2015-04-30
* Updated DreamFactory to 1.9.2
* Enabled mssql module for DreamFactory modules
* Updated phpMyAdmin to 4.4.3
* Updated PHP to 5.4.40

Version 1.9.1-1      2015-03-31
* Updated DreamFactory Admin Console to 1.0.10
* Updated OpenSSL to 1.0.1m
* Updated PHP to 5.4.39

Version 1.9.1-0      2015-03-20
* Updated Dreamfactory to 1.9.1
* Updated OpenSSL to 1.0.1l
* Fixed binaries for Git
* Updated PHP to 5.4.38

Version 1.9.0-1      2015-02-13
* Updated PHP to 5.4.37
* Updated Apache to 2.4.12
* Updated MySQL to 5.5.42 for Linux and Windows
* Updated PageSpeed to 1.9.32.3

Version 1.9.0-0      2015-02-03
* Updated DreamFactory to 1.9.0
* Updated phpMyAdmin to 4.3.7
* Updated OpenSSL to 1.0.1k (Linux and OS X)
* Updated PHP to 5.4.36
* Updated Git to 1.9.5

Version 1.8.2-0      2014-11-24
* Updated DreamFactory to 1.8.2
* Fix MySQL startup issue for OS X Yosemite
* Updated PHP to 5.4.34
* Updated OpenSSL to 1.0.1j

Version 1.8.0-0      2014-10-18
* Updated DreamFactory to 1.8.0
* Added OCI8 module. It requires InstantClient 11.2.
* Updated phpMyAdmin to 4.2.9.1
* Updated MySQL to 5.5.40
* Updated PHP to 5.4.33

Version 1.7.8-0      2014-09-11
* Updated phpMyAdmin to 4.2.8
* Updated DreamFactory to 1.7.8
* Updated MySQL to 5.5.39
* Updated PHP to 5.4.32
* Updated OpenSSL to 1.0.1i
* Updated phpMyAdmin to 4.2.7.1

Version 1.7.6-0      2014-08-18
* Updated DreamFactory to 1.7.6
* Updated Apache to 2.4.10
* Updated PHP to 5.4.31
* Updated MySQL to 5.5.38
* Updated XDebug to 2.2.5
* Updated XCache to 3.1.0
* Updated mongo-php-driver to 1.5.4
* Updated php-couchbase to 1.1.5
* Updated Libxslt to 1.1.28
* Updated Libxml2 to 2.9.1

Version 1.6.10-0      2014-07-03
* Updated DreamFactory to 1.6.10
* Updated PHP to 5.4.30
* Updated phpMyAdmin to 4.2.5
* Updated OpenSSL to 1.0.1h

Version 1.5.9-1      2014-05-14
* Update DreamFactory SDK For PHP to v1.5.12

Version 1.5.9-0      2014-05-01
* Update DreamFactory to 1.5.9

Version 1.5.8-0      2014-04-30
* Update DreamFactory to 1.5.8
* Added libv8 and v8js PHP extension for Linux and OS X

Version 1.4.0-2      2014-04-16
* Fixed an issue with X-Frame-Options configuration

Version 1.4.0-1      2014-04-08
* Updated OpenSSL to 1.0.1g
* Updated phpMyAdmin to 4.1.12
* Improved Apache configuration for PHP-FPM mode
* Updated Apache to 2.4.9
* Updated Git to 1.9.0
* Updated PHP to 5.4.26
* Updated phpMyAdmin to 4.1.9

Version 1.4.0-0      2014-03-05
* Updated DreamFactory to 1.4.0
* Updated PostgreSQL to 9.3.3
* Updated MySQL to 5.5.36

Version 1.3.3-5      2014-02-07
* Added PHP extensions for MSSQL and PostgreSQL

Version 1.3.3-2      2014-01-30
* Disable default mail from installer

Version 1.3.3-1      2014-01-16
* Added First Name and Last Name configuration
* Updated platform libraries
* Updated phpMyAdmin to 4.1.4
* Updated PHP to 5.4.24
* Updated OpenSSL to 1.0.1f in Linux and OSX

Version 1.3.3-0      2014-01-07
* Updated DreamFactory to 1.3.3
* Updated phpMyAdmin to 4.1.0
* Updated PHP to 5.4.23

Version 1.2.3-0      2013-12-12
* Updated DreamFactory to 1.2.3

Version 1.2.1-0      2013-12-11
* Updated DreamFactory to 1.2.1

Version 1.2.0-0      2013-12-10
* Updated DreamFactory to 1.2.0

Version 1.1.5-0      2013-12-04
* Updated DreamFactory to 1.1.5
* Updated PHP to 5.4.22
* Updated phpMyAdmin to 4.0.9

Version 1.1.3-0      2013-11-11
* Initial release, bundles Apache 2.4.6, PHP 5.4.21, MySQL 5.5.32 and
DreamFactory 1.1.3
