<?php
//createdb.php
//
//script used to create the necessary databases to run dreamfactory
//it won't overwrite any existent database

if ($db=($GLOBALS["___mysqli_ston"] = mysqli_connect('localhost', 'root', '5462198', null, 3308))) {
    mysqli_query($GLOBALS["___mysqli_ston"], "create database bitnami_dreamfactory collate utf8_general_ci");
    mysqli_query($GLOBALS["___mysqli_ston"], "GRANT ALL PRIVILEGES ON bitnami_dreamfactory.* TO 'bn_dreamfactory'@'localhost' IDENTIFIED BY 'b19599cb12'");
    mysqli_query($GLOBALS["___mysqli_ston"], "GRANT ALL PRIVILEGES ON bitnami_dreamfactory.* TO 'bn_dreamfactory'@'127.0.0.1' IDENTIFIED BY 'b19599cb12'");
    mysqli_query($GLOBALS["___mysqli_ston"], "flush privileges");
}
else {
    die("There was an error creating the database or setting the appropriate privileges");
}
?>