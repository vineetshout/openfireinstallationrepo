#!/bin/sh
LDFLAGS="-L/opt/dreamfactory-2.8.0-3/common/lib $LDFLAGS"
export LDFLAGS
CFLAGS="-I/opt/dreamfactory-2.8.0-3/common/include $CFLAGS"
export CFLAGS
CXXFLAGS="-I/opt/dreamfactory-2.8.0-3/common/include $CXXFLAGS"
export CXXFLAGS
		    
PKG_CONFIG_PATH="/opt/dreamfactory-2.8.0-3/common/lib/pkgconfig"
export PKG_CONFIG_PATH
