#!/bin/sh
echo $PATH | egrep "/opt/dreamfactory-2.8.0-3/common" > /dev/null
if [ $? -ne 0 ] ; then
PATH="/opt/dreamfactory-2.8.0-3/sqlite/bin:/opt/dreamfactory-2.8.0-3/git/bin:/opt/dreamfactory-2.8.0-3/postgresql/bin:/opt/dreamfactory-2.8.0-3/php/bin:/opt/dreamfactory-2.8.0-3/mysql/bin:/opt/dreamfactory-2.8.0-3/mongodb/bin:/opt/dreamfactory-2.8.0-3/nginx/sbin:/opt/dreamfactory-2.8.0-3/nodejs/bin:/opt/dreamfactory-2.8.0-3/apache2/bin:/opt/dreamfactory-2.8.0-3/common/bin:$PATH"
export PATH
fi
echo $LD_LIBRARY_PATH | egrep "/opt/dreamfactory-2.8.0-3/common" > /dev/null
if [ $? -ne 0 ] ; then
LD_LIBRARY_PATH="/opt/dreamfactory-2.8.0-3/sqlite/lib:/opt/dreamfactory-2.8.0-3/git/lib:/opt/dreamfactory-2.8.0-3/postgresql/lib:/opt/dreamfactory-2.8.0-3/mysql/lib:/opt/dreamfactory-2.8.0-3/mongodb/lib:/opt/dreamfactory-2.8.0-3/nginx/lib:/opt/dreamfactory-2.8.0-3/apache2/lib:/opt/dreamfactory-2.8.0-3/common/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
fi

TERMINFO=/opt/dreamfactory-2.8.0-3/common/share/terminfo
export TERMINFO
##### SQLITE ENV #####
			
##### GIT ENV #####
GIT_EXEC_PATH=/opt/dreamfactory-2.8.0-3/git/libexec/git-core/
export GIT_EXEC_PATH
GIT_TEMPLATE_DIR=/opt/dreamfactory-2.8.0-3/git/share/git-core/templates
export GIT_TEMPLATE_DIR
GIT_SSL_CAINFO=/opt/dreamfactory-2.8.0-3/common/openssl/certs/curl-ca-bundle.crt
export GIT_SSL_CAINFO

SASL_CONF_PATH=/opt/dreamfactory-2.8.0-3/common/etc
export SASL_CONF_PATH
SASL_PATH=/opt/dreamfactory-2.8.0-3/common/lib/sasl2 
export SASL_PATH
LDAPCONF=/opt/dreamfactory-2.8.0-3/common/etc/openldap/ldap.conf
export LDAPCONF
##### PHP ENV #####
PHP_PATH=/opt/dreamfactory-2.8.0-3/php/bin/php
export PHP_PATH
##### MYSQL ENV #####

##### MONGODB ENV #####

      ##### NGINX ENV #####

##### NODEJS ENV #####

export NODE_PATH=/opt/dreamfactory-2.8.0-3/nodejs/lib/node_modules

            ##### APACHE ENV #####

##### FREETDS ENV #####
FREETDSCONF=/opt/dreamfactory-2.8.0-3/common/etc/freetds.conf
export FREETDSCONF
FREETDSLOCALES=/opt/dreamfactory-2.8.0-3/common/etc/locales.conf
export FREETDSLOCALES
##### CURL ENV #####
CURL_CA_BUNDLE=/opt/dreamfactory-2.8.0-3/common/openssl/certs/curl-ca-bundle.crt
export CURL_CA_BUNDLE
##### SSL ENV #####
SSL_CERT_FILE=/opt/dreamfactory-2.8.0-3/common/openssl/certs/curl-ca-bundle.crt
export SSL_CERT_FILE
OPENSSL_CONF=/opt/dreamfactory-2.8.0-3/common/openssl/openssl.cnf
export OPENSSL_CONF
OPENSSL_ENGINES=/opt/dreamfactory-2.8.0-3/common/lib/engines
export OPENSSL_ENGINES


. /opt/dreamfactory-2.8.0-3/scripts/build-setenv.sh
