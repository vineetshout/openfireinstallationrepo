#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/opt/dreamfactory-2.8.0-3/common/lib"
XSLT_LIBS="-lxslt  -L/opt/dreamfactory-2.8.0-3/common/lib -lxml2 -L/opt/dreamfactory-2.8.0-3/common/lib -lz -liconv -lm -ldl -lm "
XSLT_INCLUDEDIR="-I/opt/dreamfactory-2.8.0-3/common/include"
MODULE_VERSION="xslt-1.1.29"
